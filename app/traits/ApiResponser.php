<?php
namespace App\Traits;

//use Illuminate\Database\Eloquent\Collection;
use illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponser {
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code = 422)
    {
        return response()->json(['message' => $message, 'code' => $code], $code);
    }

    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty()) {
            return response()->json(['data' => $collection], $code);
        }

        $collection = $this->sortData($collection);

//        if ($collection->first()->transformer) {
//            $transformer = $collection->first()->transformer;
//            $collection = $this->transformData($collection, $transformer);
//            return $this->successResponse($collection, $code);
//        }

        return $this->successResponse(['data' => $collection], $code);
    }

    protected function showOne(Model $model, $code = 200)
    {
        if (!$model->transformer) {
            return $this->successResponse(['data' => $model], $code);
        }

        $transformer = $model->transformer;
        $model = $this->transformData($model, $transformer);
        return $this->successResponse(['data' => $model], $code);
    }

    protected function showMessage($message, $code = 200)
    {
        return $this->successResponse(['data' => $message], $code);
    }

    private function sortData(Collection $collection)
    {
        if (request()->has('sort_by')) {
            $attribute = request()->sort_by;
            $collection = $collection->sortBy->{$attribute};
        }

        return $collection;
    }

    private function transformData($data, $transformer)
    {
        $transformation = fractal($data, new $transformer);
        return $transformation->toArray();
    }
}

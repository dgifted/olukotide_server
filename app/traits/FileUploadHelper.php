<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

trait FileUploadHelper
{
    /*
     *  $type: [image, document, others] as stipulated in the Upload model class
     */

    protected function storeFile($file, $type = 'image', $disk = 'public')
    {
        $fileName = Str::random(8) . $file->getClientOriginalName();

        try {
            if ($type !== 'image') {
                Storage::disk($disk)->put($fileName, $file);
            } else {
                $image = Image::make($file);
                $image->save($fileName);
                Storage::disk($disk)->put($fileName, $image);
            }

            return $fileName;

        } catch (\Throwable $e) {
            //TODO log exception
            return null;
        }
    }

    protected function removeStoredFile($fileName, $disk = 'public')
    {
        if (!Storage::disk($disk)->exists($fileName)) {
            return true;
        }

        try {
            Storage::disk($disk)->delete($fileName);
            return true;
        } catch (\Throwable $e) {
            //TODO log exception
            return false;
        }
    }
}

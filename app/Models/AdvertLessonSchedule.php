<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdvertLessonSchedule extends Model
{
    use HasFactory;
    const DAYS = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    const SHIFTS = ['morning', 'afternoon', 'after-school', 'evening'];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function advert()
    {
        return $this->belongsTo(Advert::class, 'advert_id');
    }
}

<?php

namespace App\Models;

use App\Scopes\TutorScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tutor extends User
{
    use HasFactory;

    protected $table = 'users';
    protected $with = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TutorScope);
    }

    public function adverts()
    {
        return $this->hasMany(Advert::class, 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonSession extends Model
{
    use HasFactory;

    const SESSION_CANCELLED = 0;
    const SESSION_PENDING = 1;
    const SESSION_IN_PROGRESS = 2;
    const SESSION_COMPLETED = 3;


    protected $guarded = ['id'];

    public function lessonRequest()
    {
        return $this->belongsTo(LessonRequest::class, 'lesson_request_id');
    }
}

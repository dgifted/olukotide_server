<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    const FILE_TYPES = [
        'image' => ['jpeg', 'jpg', 'png', 'gif', 'tiff'],
        'documents' => ['pdf', 'rtf', 'doc', 'docx'],
        'others' => []
    ];

    public function uploadable()
    {
        return $this->morphTo(__FUNCTION__, 'uploadable_type', 'uploadable_id');
    }
}

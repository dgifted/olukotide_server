<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;
use willvincent\Rateable\Rateable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, LogsActivity, Rateable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'uid',
        'email',
        'date_of_birth',
        'password',
        'phone',
        'gender',
        'postal_address',
        'land_line',
        'skype_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'email_verified_at',
        'remember_token',
        'verification_token',
        'created_at',
        'updated_at',
        'ip_address',
        'last_login'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'avatar',
        'email_verified',
        'role',
    ];

    public static function generateUID()
    {
        return uniqid(time(), true);
    }

    public static function generateVerificationCode()
    {
        return Str::random(32);
    }

    public function getEmailVerifiedAttribute()
    {
        return $this->email_verified_at !== null;
    }

    public function getRoleAttribute()
    {
        return $this->adverts()->count() > 0 ? 'tutor' : 'student';
    }

    public function getAvatarAttribute()
    {
        return $this->uploads()->where([
                'file_type' => 'image',
                'tag' => 'avatar'
            ])->first()->upload_path ?? '';
    }

    public function adverts()
    {
        return $this->hasMany(Advert::class, 'user_id');
    }

    public function lessonRequests()
    {
        return $this->hasMany(LessonRequest::class, 'user_id');
    }

    public function favoriteTutors()
    {
        return $this->hasMany(FavoriteTutor::class, 'user_id');
    }

    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    public function preference()
    {
        return $this->hasOne(UserPreference::class, 'user_id');
    }

    public function paymentOptions()
    {
        return $this->hasMany(PaymentOption::class, 'user_id');
    }
}

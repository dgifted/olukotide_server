<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavoriteTutor extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $table = 'favorite_tutors';

    public function student()
    {
        return $this->belongsTo(Student::class, 'user_id');
    }
}

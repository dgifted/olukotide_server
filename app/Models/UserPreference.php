<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPreference extends Model
{
    use HasFactory;

    const SET_OFF = 0;
    const SET_ON = 1;

    const UPDATE_TARGETS = [
        'email_notify_activity' => 1,
        'email_notify_lesson_request' => 2,
        'email_notify_offer_ad' => 3,
        'sms_notify_lesson_request' => 4
    ];

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

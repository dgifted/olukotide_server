<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    use HasFactory;
    const CLASS_TYPE_INDIVIDUAL = 1;
    const CLASS_TYPE_GROUP = 2;

    const CLASS_PLATFORM_STUDENT = 1;
    const CLASS_PLATFORM_TUTOR = 2;
    const CLASS_PLATFORM_ONLINE = 3;

    const APPROVAL_PENDING = 0;
    const APPROVAL_DONE = 1;
    const APPROVAL_CANCELLED = 2;

    protected $guarded = ['id'];

    public function lessonRequests()
    {
        return $this->hasMany(LessonRequest::class, 'advert_id');
    }

    public function schedules()
    {
        return $this->hasMany(AdvertLessonSchedule::class, 'advert_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function tutor()
    {
        return $this->belongsTo(Tutor::class, 'user_id');
    }
}

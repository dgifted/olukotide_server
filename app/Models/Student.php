<?php

namespace App\Models;

use App\Scopes\StudentScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends User
{
    use HasFactory;

    protected $table = 'users';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StudentScope);
    }


    public function lessonRequests()
    {
        return $this->hasMany(LessonRequest::class, 'user_id');
    }

    public function favoriteTutors()
    {
        return $this->hasMany(FavoriteTutor::class, 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'student_id');
    }

}

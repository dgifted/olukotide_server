<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    public function subjectCategory()
    {
        return $this->belongsTo(SubjectCategory::class, 'subject_category_id');
    }

    public function adverts()
    {
        return $this->hasMany(Advert::class, 'subject_id');
    }
}

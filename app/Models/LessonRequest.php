<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonRequest extends Model
{
    use HasFactory;

    const STATUS_PENDING = 0;
    const STATUS_CANCELLED = 1;
    const STATUS_COMPLETED = 2;

    protected $guarded = ['id'];

    public function advert()
    {
        return $this->belongsTo(Advert::class, 'advert_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'user_id');
    }

    public function lessonSession()
    {
        return $this->hasOne(LessonSession::class, 'lesson_request_id');
    }
}

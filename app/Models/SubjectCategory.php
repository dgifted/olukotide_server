<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubjectCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'subject_category_id');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $emailPayload;

    public function __construct($emailPayload)
    {
        $this->emailPayload = $emailPayload;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->emailPayload->from)
            ->subject($this->emailPayload->subject)
            ->markdown('emails.user_register', [
                'user' => $this->emailPayload->user
            ]);
    }
}

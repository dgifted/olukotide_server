<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $emailPayload;

    public function __construct($emailPayload)
    {
        $this->emailPayload = $emailPayload;
    }

    public function build()
    {
        return $this->from($this->emailPayload->from)
            ->subject($this->emailPayload->subject)
            ->markdown('email.new_password', [
                'newPassword' => $this->emailPayload->newPassword
            ]);
    }
}

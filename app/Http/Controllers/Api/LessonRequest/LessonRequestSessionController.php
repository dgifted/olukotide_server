<?php

namespace App\Http\Controllers\Api\LessonRequest;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\LessonRequest;
use App\Models\LessonSession;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LessonRequestSessionController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(LessonRequest $lessonRequest)
    {
        $lessonSessions = $lessonRequest->lessonSession()->get();

        return $this->showAll($lessonSessions);
    }

    public function store(Request $request, LessonRequest $lessonRequest)
    {
        $user = User::findOrFail(auth()->user()->id);
        list($passed,) = $this->checkUserLessonOwnership($user, $lessonRequest);

        if (!$passed) {
            return $this->errorResponse('Does not belong to you the lesson request you want to access');
        }

        $request->validate([
            'startedAt' => ['required', 'date'],
        ]);

        $data = $request->all();
        unset($request);
        $data['scheduled_to_start'] = Carbon::parse($data['startedAt']);

        $lessonSession = $lessonRequest->lessonSession()->create($data);

        return $this->showOne($lessonSession);
    }

    public function show(LessonRequest $lessonRequest, LessonSession $lessonSession)
    {
        $user = auth()->user();

        list($passed,) = $this->checkUserLessonOwnership($user, $lessonRequest);

        if (!$passed) {
            return $this->errorResponse('Does not belong to you the session you requested!');
        }

        if (!$this->checkRequestSessionRelationship($lessonRequest, $lessonSession)) {
            return $this->errorResponse('Does not belong to you the session you requested!!');
        }

        return $this->showOne($lessonSession);
    }

    public function update(Request $request, LessonRequest $lessonRequest, LessonSession $lessonSession)
    {

        $user = User::findOrFail(auth()->user()->id);

        list($passed, $userType) = $this->checkUserLessonOwnership($user, $lessonRequest);

        if (!$passed) {
            return $this->errorResponse('Does not belong to you the session you requested!');
        }

        if (!$this->checkRequestSessionRelationship($lessonRequest, $lessonSession)) {
            return $this->errorResponse('Does not belong to you the session you requested!');
        }

        $data = $this->transformRequestInputData($request);
        unset($request);

        if ($userType === 'student') {
            if (!is_null($lessonSession->user_joined_at)) {
                $data['user_joined_at'] = $lessonSession->user_joined_at;
            } else {
                $data['user_joined_at'] = now();
            }

            $data['ended_at'] = null;
        }

        if ($userType === 'tutor') {
            if (!is_null($lessonSession->ended_at)) {
                $data['ended_at'] = $lessonSession->ended_at;
            } else {
                $data['ended_at'] = now();
            }

            $data['user_joined_at']  = null;
        }

        $lessonSession->fill($data);
        $lessonSession->save();

        return $this->showOne($lessonSession);
    }

    public function destroy(LessonRequest $lessonRequest)
    {
        // TODO pending decision & implementation.
    }

    private function transformRequestInputData(Request $request)
    {
        $requestPayload = [];

        if ($request->has('endedAt') && $request->get('endedAt')) {
            $requestPayload['ended_at'] = $request->get('endedAt');
        }

        if ($request->has('status') && $request->get('status')) {
            $requestPayload['status'] = $request->get('status');
        }

        if ($request->has('startedAt') && $request->get('startedAt')) {
            $requestPayload['started_at'] = $request->get('startedAt');
        }

        return $requestPayload;
    }

    private function checkUserLessonOwnership(User $user, LessonRequest $lessonRequest)
    {
        $flag = false;
        $userType = 'student';

        if ($user->role === 'student') {
            $studentLessonRequestArray = $user->lessonRequests()->get()->pluck('id')->toArray();
            if (in_array($lessonRequest->id, $studentLessonRequestArray)) {
                $flag = true;
            }
        } elseif ($user->role === 'tutor') {
            $tutorLessonRequest = $user->adverts()
                ->with('lessonRequests')
                ->where('id', $lessonRequest->id)
                ->first();
            if ($tutorLessonRequest) {
                $flag = true;
                $userType = 'tutor';
            }
        }

        return [$flag, $userType];
    }

    private function checkRequestSessionRelationship(LessonRequest $lessonRequest, LessonSession $lessonSession)
    {
        $requestSession = $lessonRequest->lessonSession()->first();
        return $requestSession->id === $lessonSession->id;
    }
}

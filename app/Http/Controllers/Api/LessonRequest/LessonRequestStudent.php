<?php

namespace App\Http\Controllers\Api\LessonRequest;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\LessonRequest;

class LessonRequestStudent extends BaseApiController
{
    public function index(LessonRequest $lessonRequest)
    {
        $student = $lessonRequest->student()->first();
        return $this->showOne($student);
    }
}

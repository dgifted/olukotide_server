<?php

namespace App\Http\Controllers\Api\LessonRequest;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\LessonRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LessonRequestController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $user = auth()->user();
        $lessonRequests = $user->lessonRequests()->with(['advert', 'advert.tutor'])->get();

        return $this->showAll($lessonRequests);
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'advertId' => ['required', 'numeric'],
        ]);

        $data = $request->all();
        unset($request);
        $data['advert_id'] = $data['advertId'];
        $data['scheduled_to_start'] = Carbon::parse($data['scheduledToStart']);

        $lessonRequest = $user->lessonRequests()->create($data);

        return $this->showOne($lessonRequest);
    }

    public function show(LessonRequest $lessonRequest)
    {
        $user = auth()->user();

        if (!$this->checkStudentOwnerShip($user, $lessonRequest)) {
            return $this->errorResponse('Does not belong to you the Lesson request you requested.');
        }

        $lessonRequest = $lessonRequest->with(['advert'])->first();
        return $this->showOne($lessonRequest);
    }

    public function update(Request $request, LessonRequest $lessonRequest)
    {
        $user = auth()->user();

        if (!$this->checkStudentOwnerShip($user, $lessonRequest)) {
            return $this->errorResponse('Does not belong to you the lesson request you want to modify.');
        }

        $data = $this->transformRequestInputData($request);
        $lessonRequest->fill($data);
        $lessonRequest->save();

        return $this->showOne($lessonRequest);
    }

    public function destroy(LessonRequest $lessonRequest)
    {
        $user = auth()->user();

        if (!$this->checkStudentOwnerShip($user, $lessonRequest)) {
            return $this->errorResponse('Does not belong to you the lesson request you want to delete.');
        }

        $lessonRequest->delete();

        return $this->showOne($lessonRequest);
    }

    private function checkStudentOwnerShip(User $user, LessonRequest $lessonRequest)
    {
        $userLessonRequestsArray = $user->lessonRequests()->get()->pluck('id')->toArray();

        return in_array($lessonRequest->id, $userLessonRequestsArray);
    }

    private function transformRequestInputData(Request $request)
    {
        $requestPayload = [];

        if ($request->has('scheduledToStart') && !is_null($request->get('scheduledToStart'))) {
            $requestPayload['scheduled_to_start'] = Carbon::parse($request->get('scheduledToStart'));
        }

        if ($request->has('status') && !is_null($request->get('status'))) {
            $requestPayload['status'] = $request->get('status');
        }

        if ($request->has('duration') && !is_null($request->get('duration'))) {
            $requestPayload['duration'] = $request->get('duration');
        }

        return $requestPayload;
    }
}

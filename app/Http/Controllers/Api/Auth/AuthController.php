<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\BaseApiController;
use App\Jobs\SendEmailJob;
use App\Mail\PasswordResetEmail;
use App\Mail\UserRegisteredEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->except([
            'register',
            'login',
            'verify',
            'reset'
        ]);
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'firstName' => ['required', 'string'],
            'lastName' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);
        unset($request);

        $data['name'] = $data['firstName'] . ' ' . $data['lastName'];
        $data['uid'] = User::generateUID();
        $data['password'] = Hash::make($data['password']);
        $data['verification_code'] = User::generateVerificationCode();
        $user = User::create($data);

        //Create default user preference
        $user->preference()->create([]);

        //Generate access token
//        $accessToken = $user->createToken('auth')->plainTextToken;

        // TODO use notification system to abstract this user creation email notification
        // TODO use event listeners to log the user's IP Address
        $emailJobPayload = new \stdClass();
        $emailPayload = new \stdClass();

        $emailPayload->subject = 'Your are registered successfully';
        $emailPayload->from = config('mail.from.address');
        $emailPayload->user = $user;

        $emailJobPayload->email = $user->email;
        $emailJobPayload->emailInstance = new UserRegisteredEmail($emailPayload);

        SendEmailJob::dispatchAfterResponse($emailJobPayload);

//        return response()->json([
//            'user' => $user,
//            'accessToken' => $accessToken
//        ]);
        return $this->showMessage('You have been successfully registered to '. config('app.name') . '.login to continue');
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $data['ip_address'] = $request->ip();
        $user = User::where('email', $data['email'])->first();

        if (!$user) {
            return $this->errorResponse('Email not register with any user.');
        }

        $passwordMatched = Hash::check($data['password'], $user->password);

        if (!$passwordMatched) {
            return $this->errorResponse('Password invalid');
        }

        // Check user's previous login ip address and alert user if change in ip
        if (!is_null($user->ip_address) && ($user->ip_address !== $data['ip_address'])) {
            //Send email notification
        }

        $user->ip_address = $data['ip_address'];
        $user->last_login = now();
        $user->save();

        $accessToken = $user->createToken('auth')->plainTextToken;
        // TODO use event listeners to log the user's IP Address after login
        // TODO notify the user of change in IP is its applicable through email.

        return response()->json([
            'user' => $user,
            'accessToken' => $accessToken
        ]);
    }

    public function logout()
    {
        $user = auth()->user();
        $user->tokens()->delete();

        return $this->showMessage('Successfully logged out');
    }

    public function verify($token)
    {
        $user = User::where('verification_token', $token)->first();

        if (!$user) {
            return $this->errorResponse('Verification link has expired. Please use the resend link to get new verification link in your inbox', 404);
        }

        $user->email_verified_at = now();
        $user->verfication_token = null;
        $user->save();

        return $this->showMessage('Your email is verified successfully');
    }

    public function reset(Request $request)
    {
        $data = $request->validate([
            'email' => ['required', 'email']
        ]);
        unset($request);

        $user = User::where('email', $data['email'])->first();

        if (!$user) {
            return $this->errorResponse('Does not exist any user with the email', 404);
        }

        if (!$this->sendResetEmail($user)) {
            return $this->errorResponse('An unexpected error occurred please try again', 500);
        }

        $tempPassword = Str::random(10);
        $user->password = Hash::make($tempPassword);
        $user->save();

        DB::table('password_resets')->insert([
            'email' => $user->email,
            'temp_password' => $tempPassword,
            'created_at' => now()
        ]);

        //TODO send out the {$tempPassword} to user email.

        return $this->showMessage('A temporary password has been sent to your inbox. Please login with the password and update your password as soon as possible.');
    }

    public function setNewPassword(Request $request)
    {
        $user = auth()->user();

        $data = $request->validate([
            'oldPassword' => ['required', 'min:8'],
            'newPassword' => ['required', 'min:8', 'confirmed']
        ]);
        unset($request);

        if (!Hash::check($data['oldPassword'], $user->password)) {
            return $this->errorResponse('Password does not match the current password. Please use reset password link.');
        }

        $user->password = Hash::make($data['password']);
        $user->save();

        return $this->showMessage('Password update successful');
    }

    private function sendResetEmail(User $user) //Boolean
    {
        $success = false;

        try {
            $password = Str::random('10');
            $user->update([
                'password' => Hash::make($password)
            ]);

            $details = new \stdClass();
            $emailPayload = new \stdClass();

            $emailPayload->subject = 'Your temporary password.';
            $emailPayload->from = config('mail.from.address');
            $emailPayload->newPassword = $password;

            $details->email = $user->email;
            $details->emailInstance = new PasswordResetEmail($emailPayload);

            SendEmailJob::dispatchAfterResponse($details);
            $success = true;
        } catch (\Throwable $exception) {
            //TODO Log exception
        }

        return $success;
    }
}

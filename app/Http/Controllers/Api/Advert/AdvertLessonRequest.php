<?php

namespace App\Http\Controllers\Api\Advert;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Advert;

class AdvertLessonRequest extends BaseApiController
{
    public function index(Advert $advert)
    {
        $lessonRequests = $advert->lessonRequests()->with('student')->get();
        return $this->showAll($lessonRequests);
    }

    public function show(Advert $advert)
    {

    }
}

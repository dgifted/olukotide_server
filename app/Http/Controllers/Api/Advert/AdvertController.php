<?php

namespace App\Http\Controllers\Api\Advert;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Advert;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Request;

class AdvertController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->except([
            'index',
            'show',
        ]);
    }

    public function index()
    {
        $adverts = Advert::with(['tutor:id,name'])->get();

        return $this->showAll($adverts);
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'title' => ['required', 'unique:adverts'],
            'subjectId' => ['required']
        ]);

        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['subject_id'] = $data['subjectId'];
        unset($request);

//        $advert = Advert::create([
//            'user_id' => $user->id,
//            'title' => $request->get('title'),
//            'subject_id'
//        ]);
        $advert = Advert::create($data);

        return $this->showOne($advert);
    }

    public function show(Advert $advert)
    {
        return $this->showOne($advert);
    }

    public function update(Request $request, Advert $advert)
    {
        $user = auth()->user();

        if (!$this->checkAdvertOwnership($user, $advert)) {
            return $this->errorResponse('Does not belong to you the ad you are attempting to modify.');
        }

        $data = $this->transformRequestInputs($request);

        $advert->update($data);

        return $this->showOne($advert);
    }

    public function destroy(Advert $advert)
    {
        $user = auth()->user();
        if (!$this->checkAdvertOwnership($user, $advert)) {
            return $this->errorResponse('Does not belong to you the ad you are attempting to modify.');
        }

        $advert->delete();
        return $this->showMessage('Ad deleted');
    }

    private function checkAdvertOwnership(User $user, Advert $advert)
    {
        $userAdvertsArray = $user->adverts()->get()->pluck('id')->toArray();

        return in_array($advert->id, $userAdvertsArray);
    }

    private function transformRequestInputs(Request $request)
    {
        $returnPayload = [];

        if ($request->has('title') && !is_null($request->get('title'))) {
            $returnPayload['title'] = $request->get('title');
        }
        if ($request->has('classType') && !is_null($request->get('classType'))) {
            $returnPayload['class_type'] = $request->get('classType');
        }
        if ($request->has('tutorBackground') && !is_null($request->get('tutorBackground'))) {
            $returnPayload['tutor_background_info'] = $request->get('tutorBackground');
        }
        if ($request->has('teachingMethod') && !is_null($request->get('teachingMethod'))) {
            $returnPayload['tutor_teaching_method'] = $request->get('teachingMethod');
        }
        if ($request->has('youtubeLink') && !is_null($request->get('youtubeLink'))) {
            $returnPayload['youtube_link'] = $request->get('youtubeLink');
        }
        if ($request->has('platform') && !is_null($request->get('platform'))) {
            $returnPayload['platform'] = $request->get('platform');
        }
        if ($request->has('language') && !is_null($request->get('language'))) {
            $returnPayload['online_lesson_language'] = $request->get('language');
        }
        if ($request->has('transportFare') && !is_null($request->get('transportFare'))) {
            $returnPayload['transport_fare'] = $request->get('transportFare');
        }
        if ($request->has('rate') && !is_null($request->get('rate'))) {
            $returnPayload['rate'] = $request->get('rate');
        }
        if ($request->has('contactInfo') && !is_null($request->get('contactInfo'))) {
            $returnPayload['tutor_contact_info'] = $request->get('contactInfo');
        }
        if ($request->has('profilePhoto') && !is_null($request->get('profilePhoto'))) {
            $returnPayload['profile_photo'] = $request->get('profilePhoto');
        }

        return $returnPayload;
    }
}

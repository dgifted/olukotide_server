<?php

namespace App\Http\Controllers\Api\Tutor;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Advert;
use App\Models\Tutor;
use Illuminate\Http\Request;

class TutorAdvertController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(Tutor $tutor)
    {
        $adverts = $tutor->adverts()->with('lessonRequests')->get();
        return $this->showAll($adverts);
    }

    public function show(Tutor $tutor, Advert $advert)
    {
        if ($tutor->id !== auth()->user()->id) {
            return $this->errorResponse('You\'re not authorized to access this resource.');
        }

        $tutorAdverts = $tutor->adverts()->get()->pluck('id')->toArray();

        if (!in_array($advert->id, $tutorAdverts)) {
            return $this->errorResponse('Advert not found for the specified tutor');
        }

        $advert = Advert::with(['lessonRequests', 'lessonRequests.student', 'lessonRequests.lessonSession'])->findOrFail($advert->id);
        return $this->showOne($advert);
    }
}

<?php

namespace App\Http\Controllers\Api\Tutor;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Student;
use App\Models\Tutor;

class TutorStudentController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(Tutor $tutor)
    {
        if (auth()->user()->id !== $tutor->id) {
            return $this->errorResponse('You\'re not authorize to access this resources.');
        }

        $students = $tutor->adverts()->whereHas('lessonRequests')
            ->with('lessonRequests.student')
            ->get()
            ->pluck('lessonRequests')
            ->collapse()
            ->pluck('student')
            ->values();

        return $this->showAll($students);
    }

    public function show(Tutor $tutor, Student $student)
    {
        if (auth()->user()->id !== $tutor->id) {
            return $this->errorResponse('You\'re not authorize to access this resources.');
        }

        if (!$this->checkTutorStudentRelationship($tutor, $student)) {
            return $this->errorResponse('Student not found.');
        }

        return $this->showOne($student);
    }

    private function checkTutorStudentRelationship(Tutor $tutor, Student $student)
    {
        $tutorStudentArray = $tutor->adverts()->whereHas('lessonRequests')
            ->with('lessonRequests.student')
            ->get()
            ->pluck('lessonRequests')
            ->collapse()
            ->pluck('student.id')
            ->values()
            ->toArray();

        return in_array($student->id, $tutorStudentArray);
    }
}

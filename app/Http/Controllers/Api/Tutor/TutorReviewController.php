<?php

namespace App\Http\Controllers\Api\Tutor;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Review;
use App\Models\Student;
use App\Models\Tutor;
use Illuminate\Http\Request;

class TutorReviewController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->except(['index']);
    }

    public function index(Tutor $tutor)
    {
        $reviews = $tutor->reviews()->with(['student'])->get();

        return $this->showAll($reviews);
    }

    public function store(Request $request, Tutor $tutor)
    {
        $student = auth()->user();
        $review = Review::where([
            'student_id' => $student->id,
            'user_id' => $tutor->id
            ])->first();


        if ($review) {
            return $this->errorResponse('You have already submitted a review for this tutor');
        }

        $review = Review::create([
            'user_id' => $tutor->id,
            'comment' => $request->get('comment'),
            'student_id' => $student->id
        ]);

        return  $this->showOne($review);
    }


    public function show(Tutor $tutor, Review $review)
    {
        if (!$this->checkTutorReviewOwnership($tutor, $review)) {
            return $this->errorResponse('Does not belong to the tutor the review you requested');
        }

        return $this->showOne($review);
    }

    public function update(Request $request, Tutor $tutor, Review $review)
    {
        if (!$this->checkTutorReviewOwnership($tutor, $review)) {
            return $this->errorResponse('Does not belong to the tutor the review you requested');
        }

        $student = Student::findOrFail(auth()->user()->id);

        if (!$this->checkStudentReviewOwnership($student, $review)) {
            return $this->errorResponse('You do not have the right to update this review.');
        }

        $review->fill($this->transformRequestInputData($request));

        if (!$review->isDirty()) {
            return $this->errorResponse('Review not changed');
        }

        $review->save();

        return $this->showOne($review);
    }

    public function destroy(Tutor $tutor, Review $review)
    {
        $student = Student::findOrFail(auth()->user()->id);

        if (!$this->checkTutorReviewOwnership($tutor, $review)) {
            return $this->errorResponse('Does not belong to the tutor the review you requested');
        }

        if (!$this->checkStudentReviewOwnership($student, $review)) {
            return $this->errorResponse('You do not have the right to delete this review.');
        }

        $review->delete();

        return $this->showMessage('Review deleted.');
    }

    private function checkTutorReviewOwnership(Tutor $tutor, Review $review)
    {
        $tutorReviewArray = $tutor->reviews()->get('id')->pluck(['id'])->toArray();
        return in_array($review->id, $tutorReviewArray);
    }

    private function checkStudentReviewOwnership(Student $student, Review $review)
    {
        $studentReviewArray = $student->reviews()->get('id')->pluck(['id'])->toArray();
        return in_array($review->id, $studentReviewArray);
    }

    private function transformRequestInputData(Request $request)
    {
        return ($request->has('comment') && !is_null($request->get('comment')))
            ? ['comment' => $request->get('comment')]
            : [];
    }
}

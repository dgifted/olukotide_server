<?php

namespace App\Http\Controllers\Api\Tutor;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\LessonRequest;
use App\Models\Tutor;
use Illuminate\Http\Request;

class TutorLessonRequestController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(Tutor $tutor)
    {
        $lessonRequests = $tutor->adverts()
            ->whereHas('lessonRequests')
            ->with('lessonRequests')
            ->get()
            ->pluck('lessonRequests')
            ->collapse();

        return $this->showAll($lessonRequests);
    }

    public function show(Tutor $tutor, LessonRequest $lessonRequest)
    {
        if (!$this->checkTutorLessonRequestOwnership($tutor, $lessonRequest)) {
            return $this->errorResponse('Lesson request does not belong to this tutor');
        }

        $lessonRequest = $lessonRequest->with(['student', 'lessonSession'])->first();
        return $this->showOne($lessonRequest);
    }

    public function update(Request $request, Tutor $tutor)
    {
        //
    }

    private function checkTutorLessonRequestOwnership(Tutor $tutor, LessonRequest $lessonRequest)
    {
        $tutorLessonRequestsCollection = $tutor->adverts()
            ->whereHas('lessonRequests')
            ->with('lessonRequests')
            ->get()
            ->pluck('lessonRequests')
            ->collapse();

        $filtered = $tutorLessonRequestsCollection->filter(function ($lReq) use ($lessonRequest) {
            return $lReq->id === $lessonRequest->id;
        });

        return $filtered->count() > 0;
    }

}

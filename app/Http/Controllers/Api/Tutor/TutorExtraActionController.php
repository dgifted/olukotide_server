<?php

namespace App\Http\Controllers\Api\Tutor;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Student;
use App\Models\Tutor;
use Illuminate\Http\Request;

class TutorExtraActionController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function rateTutor(Request $request)
    {
        $student = Student::findOrFail(auth()->user()->id);

        if (!$request->has('tutorId') || is_null($request->get('tutorId'))) {
            return $this->errorResponse('An error occurred. Please try again.');
        }

        if (!$request->has('ratingValue') || is_null($request->get('ratingValue'))) {
            return $this->errorResponse('An error occurred. Please try again.');
        }

        $tutor = Tutor::findOrFail($request->get('tutorId'));

        if (!$this->checkStudentEnrolment($student, $tutor)) {
            return $this->errorResponse('You may rate only the tutors you have enrolled with.');
        }


        $tutor->rateOnce((double)$request->get('ratingValue'));

        return [
            'avgRating' => (double)$tutor->averageRating,
            'totalRating' => (double)$tutor->ratings()->count(),
            'totalUserRated' => (double)$tutor->usersRated()
        ];
    }

    private function checkStudentEnrolment(Student $student, Tutor $tutor)
    {
        $tutorsArray = $student->lessonRequests()->with(['advert.tutor'])
            ->get()
            ->pluck('advert.tutor')
            ->unique('id')
            ->values()
            ->pluck('id')
            ->toArray();

        return in_array($tutor->id, $tutorsArray);
    }
}

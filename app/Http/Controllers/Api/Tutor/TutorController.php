<?php

namespace App\Http\Controllers\Api\Tutor;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Tutor;
//use Illuminate\Http\Request;

class TutorController extends BaseApiController
{
    public function index()
    {
        $tutors = Tutor::with([
            'adverts'
        ])->get()
        ->each(function ($tutor) {
            $tutor->rating = $tutor->ratings()->count();
        });

        return $this->showAll($tutors);
    }

    public function show(Tutor $tutor)
    {
        $tutor = $tutor->with('adverts')->first();
        $tutor->rating = $tutor->ratings()->count();
        return $this->showOne($tutor);
    }
}

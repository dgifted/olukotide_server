<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\UserPreference;
use App\Traits\FileUploadHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserExtraActionController extends BaseApiController
{
    use FileUploadHelper;

    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getUserDetail()
    {
        $user = auth()->user();

        return $this->showOne($user);
    }

    public function profileUpdate(Request $request)
    {
        $user = auth()->user();

        $data = $this->transformRequestInputForProfileUpdate($request);
        if (isset($data['firstName']) && isset($data['lastName'])) {
            $data['name'] = $data['firstName'] . ' ' . $data['lastName'];
        }
        $user->fill($data);
        $user->save();

        return $this->showOne($user);
    }

    public function uploadAvatar(Request $request)
    {
        $user = auth()->user();

        if (!$request->has('avatar') || is_null($request->file('avatar'))) {
            return $this->errorResponse('Image file is either not present or invalid.');
        }

        //Check if there is an avatar and remove it before storing the new one.
        $oldAvatar = $user->uploads()->where([
            'file_type' => 'image',
            'tag' => 'avatar'
        ])->first();

        $flag = true;

        if ($oldAvatar) {
            $flag = $this->removeStoredFile($oldAvatar->upload_path);
        }

        if (!$flag) {
            return $this->errorResponse('We could not change your profile avatar at the moment please try again.');
        }

        $file = $request->file('avatar');
        $uploadedFilePath = $this->storeFile($file, 'image');

        if (!$uploadedFilePath) {
            return $this->errorResponse('File upload failed. Please try again');
        }

        //Delete users uploaded avatar
        if ($oldAvatar) {
            $oldAvatar->delete();
        }

        $avatar = $user->uploads()->create([
            'upload_path' => $uploadedFilePath,
            'file_type' => 'image',
            'tag' => 'avatar'
        ]);

        return $this->showOne($avatar);
    }

    public function removeAvatar()
    {
        $user = auth()->user();
        $operationSucceeded = false;

        $user->uploads()->where([
            'file_type' => 'image',
            'tag' => 'avatar'
        ])->get()
            ->each(function ($avatar) use (&$operationSucceeded, $user) {
                $operationSucceeded = $this->removeStoredFile($avatar->upload_path);
                if ($operationSucceeded) {
                    $avatar->delete();
                }
            });

        return $operationSucceeded ? $this->showMessage('Done') : $this->errorResponse('Failed');
    }

    public function uploadIDDocument(Request $request)
    {
        // The document may be an image snapshot actual document
        $user = auth()->user();

        if (!$request->has('idDoc') || is_null($request->file('idDoc'))) {
            return $this->errorResponse('Image file is either not present or invalid.');
        }


        $file = $request->file('idDoc');
        $uploadedFilePath = $this->storeFile($file, 'image');

        if (!$uploadedFilePath) {
            return $this->errorResponse('File upload failed. Please try again');
        }

        $identification = $user->uploads()->create([
            'upload_path' => $uploadedFilePath,
            'file_type' => 'image',
            'tag' => 'id'
        ]);

        return $this->showOne($identification);
    }

    public function setUserPreference(Request $request)
    {
        $user = auth()->user();
        $preference = $user->preference()->firstOrCreate([]);
        $target = $request->get('target');

        switch ($target) {
            case UserPreference::UPDATE_TARGETS['email_notify_activity']:
                if (!$this->singleFieldUpdate($preference, 'email_notify_activity', $request->get('emailNotifyActivity'))) {
                    return $this->errorResponse('Error updating your preference');
                }
                return (string)$preference->email_notify_activity;

            case UserPreference::UPDATE_TARGETS['email_notify_lesson_request']:
                if (!$this->singleFieldUpdate($preference, 'email_notify_lesson_request', $request->get('emailNotifyLessonRequest'))) {
                    return $this->errorResponse('Error updating your preference');
                }
                return (string)$preference->email_notify_lesson_request;

            case UserPreference::UPDATE_TARGETS['email_notify_offer_ad']:
                if (!$this->singleFieldUpdate($preference, 'email_notify_offer_ad', $request->get('emailNotifyOfferAd'))) {
                    return $this->errorResponse('Error updating your preference');
                }
                return (string)$preference->email_notify_offer_ad;

            case UserPreference::UPDATE_TARGETS['sms_notify_lesson_request']:
                if (!$this->singleFieldUpdate($preference, 'sms_notify_lesson_request', $request->get('smsNotifyLessonRequest'))) {
                    return $this->errorResponse('Error updating your preference');
                }
                return (string)$preference->sms_notify_lesson_request;

            default:
                $payload = $this->prepareRequestInputForUserPreference($request);
                if (!$this->bulkFieldsUpdate($preference, $payload)) {
                    return $this->errorResponse('Error updating your preference');
                }
                return $this->showOne($preference);
        }
    }

    public function getUserPreference()
    {
        $user = auth()->user();
        $preference = $user->preference()->first();

        return $this->showOne($preference);
    }

    private function singleFieldUpdate(UserPreference $preference, $fieldName, $value)
    {
        $preferenceObjectKeys = $preference->getAttributes();

        if (!in_array($fieldName, $preferenceObjectKeys)) {
            return false;
        }

        $preference->{$fieldName} = $value;
        $preference->save();

        return true;
    }

    private function bulkFieldsUpdate(UserPreference $preference, array $payload)
    {

        try {
            $preference->update($payload);
            return true;
        } catch (\Throwable $e) {
            //TODO log error
            return false;
        }
    }

    private function prepareRequestInputForUserPreference(Request $request)
    {
        $payload = [];

        if ($request->has('emailNotifyActivity') && !is_null($request->get('emailNotifyActivity'))) {
            $payload['email_notify_activity'] = $request->get('emailNotifyActivity');
        }
        if ($request->has('emailNotifyLessonRequest') && !is_null($request->get('emailNotifyLessonRequest'))) {
            $payload['email_notify_lesson_request'] = $request->get('emailNotifyLessonRequest');
        }
        if ($request->has('emailNotifyOfferAd') && !is_null($request->get('emailNotifyOfferAd'))) {
            $payload['email_notify_offer_ad'] = $request->get('email_notify_offer_ad');
        }
        if ($request->has('smsNotifyLessonRequest') && !is_null($request->get('smsNotifyLessonRequest'))) {
            $payload['sms_notify_lesson_request'] = $request->get('smsNotifyLessonRequest');
        }

        return $payload;
    }

    private function transformRequestInputForProfileUpdate(Request $request)
    {
        $payload = [];

        if ($request->has('firstName') && !is_null($request->get('firstName'))) {
            $payload['firstName'] = $request->get('firstName');
        }
        if ($request->has('lastName') && !is_null($request->get('lastName'))) {
            $payload['lastName'] = $request->get('lastName');
        }
        if ($request->has('phone') && !is_null($request->get('phone'))) {
            $payload['phone'] = $request->get('phone');
        }
        if ($request->has('gender') && !is_null($request->get('gender'))) {
            $payload['gender'] = $request->get('gender');
        }
        if ($request->has('postalAddress') && !is_null($request->get('postalAddress'))) {
            $payload['postal_address'] = $request->get('postalAddress');
        }
        if ($request->has('landLine') && !is_null($request->get('landLine'))) {
            $payload['land_line'] = $request->get('landLine');
        }
        if ($request->has('skypeId') && !is_null($request->get('skypeId'))) {
            $payload['skype_id'] = $request->get('skypeId');
        }
        if ($request->has('dateOfBirth') && !is_null($request->get('dateOfBirth'))) {
            $payload['date_of_birth'] = Carbon::parse($request->get('dateOfBirth'));
        }

        return $payload;
    }
}

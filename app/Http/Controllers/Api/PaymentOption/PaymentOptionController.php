<?php

namespace App\Http\Controllers\Api\PaymentOption;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\PaymentOption;
use App\Models\User;
use Illuminate\Http\Request;

class PaymentOptionController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $user = auth()->user();

        $options = $user->paymentOptions()->get();

        return $this->showAll($options);
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'cardName' => ['required', 'string'],
            'cardNumber' => ['required', 'string', 'min:16'],
            'cardExpDate' => ['required'],
            'cvv' => ['required']
        ]);

        $data = $request->all();
        unset($request);

        $option = $user->paymentOptions()->create([
            'card_name' => $data['cardName'],
            'card_number' => $data['cardNumber'],
            'exp_date' => $data['cardExpDate'],
            'cvv' => $data['cvv']
        ]);

        return $this->showOne($option);
    }

    public function show(PaymentOption $paymentOption)
    {
        $user = auth()->user();

        if (!$this->checkUserOptionOwnership($user, $paymentOption)) {
            return $this->errorResponse('Payment option not found.', 404);
        }

        return $this->showOne($paymentOption);
    }

    public function destroy(PaymentOption $paymentOption)
    {
        $user = auth()->user();

        if (!$this->checkUserOptionOwnership($user, $paymentOption)) {
            return $this->errorResponse('Payment option not found.', 404);
        }

        $paymentOption->delete();

        return $this->showMessage('Payment option deleted.');
    }

    private function checkUserOptionOwnership(User $user, PaymentOption $option)
    {

        $userOptionsArray = $user->paymentOptions()->get('id')->pluck('id')->toArray();

        return in_array($option->id, $userOptionsArray);
    }
}

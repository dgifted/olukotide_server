<?php

namespace App\Http\Controllers\Api\Student;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\FavoriteTutor;
use App\Models\Tutor;
use Illuminate\Http\Request;

class StudentFavoriteTutorController extends BaseApiController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $student = auth()->user();
        $favoriteTutors = $student->favoriteTutors()
            ->get(['id', 'tutor_id'])
            ->map(function ($favTutor) {
                return Tutor::find($favTutor->tutor_id);
            });

        return $this->showAll($favoriteTutors);
    }

    public function store(Request $request)
    {
        if (!$request->has('tutorId') || is_null($request->get('tutorId'))) {
            return $this->errorResponse('tutor id is required');
        }

        $tutor = Tutor::with(['lessonRequests'])->findOrFail($request->get('tutorId'));

        $user = auth()->user();
        $favedTutorsArray = $user->favoriteTutors()->get()->pluck('tutor_id')->toArray();

        if (in_array($tutor->id, $favedTutorsArray)) {
            $favoriteTutor = FavoriteTutor::where([
                'user_id' => $user->id,
                'tutor_id' => $tutor->id
            ])->firstOrFail();

            $favoriteTutor->delete();

            return $this->showMessage('Tutor has been removed from your favorite tutors list.');
        }

        $user->favoriteTutors()->create([
            'tutor_id' => $tutor->id
        ]);

        return $this->showMessage('Tutor has been added to your favorite tutors list');
    }

    public function destroy(FavoriteTutor $favoriteTutor)
    {
        $favoriteTutor->delete();
        return $this->showMessage('Tutor has been removed from your favorite tutors list.');
    }
}

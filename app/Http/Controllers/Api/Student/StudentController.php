<?php

namespace App\Http\Controllers\Api\Student;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends BaseApiController
{
    public function index()
    {
        $students = Student::with(['lessonRequests'])->get();
        return $this->showAll($students);
    }

    public function show(Student $student)
    {
        return $this->showOne($student);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Model;

class BaseApiController extends Controller
{
    use ApiResponser;

}

<?php

namespace App\Http\Controllers\Api\SubjectCategory;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\SubjectCategory;

class SubjectCategoryController extends BaseApiController
{
    public function index()
    {
        $categories = SubjectCategory::all();
        return $this->showAll($categories);
    }

    public function show(SubjectCategory $subjectCategory)
    {
        $category = $subjectCategory->with(['subjects'])->find($subjectCategory->id);
        return $this->showOne($category);
    }
}

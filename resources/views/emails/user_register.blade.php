@component('mail::message')
# You are account has been successfully created.

Dear {{$user->name}},
Welcome to {{ config('app.name') }}.
<p>You are account has been successfully created.</p>

@component('mail::button', ['url' => ''])
Click to verify your account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

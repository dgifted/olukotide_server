@component('mail::message')
    # New Password

    Below is your new password.

    {{$newPassword}}

    <p>It's recommended that you change this password as soon as possible.</p>

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent

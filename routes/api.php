<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return 'Welcome to Olukotide';
});

Route::prefix('auth')->group(function () {
    Route::post('/register', [App\Http\Controllers\Api\Auth\AuthController::class, 'register']);
    Route::post('/login', [App\Http\Controllers\Api\Auth\AuthController::class, 'login']);
    Route::get('/logout', [App\Http\Controllers\Api\Auth\AuthController::class, 'logout']);
    Route::get('/verify/{token}', [App\Http\Controllers\Api\Auth\AuthController::class, 'verify']);
    Route::post('/reset', [App\Http\Controllers\Api\Auth\AuthController::class, 'reset']);
    Route::post('/set-password', [App\Http\Controllers\Api\Auth\AuthController::class, 'setNewPassword']);
});
Route::apiResource('/adverts', App\Http\Controllers\Api\Advert\AdvertController::class);
Route::apiResource('/tutors', App\Http\Controllers\Api\Tutor\TutorController::class)->only(['index', 'show']);
Route::apiResource('/tutors.lessonRequests', App\Http\Controllers\Api\Tutor\TutorLessonRequestController::class)->only(['index', 'show', 'update']);
Route::apiResource('/students', App\Http\Controllers\Api\Student\StudentController::class)->only(['index', 'show']);
Route::apiResource('/subjectCategories', App\Http\Controllers\Api\SubjectCategory\SubjectCategoryController::class)->only(['index', 'show']);
Route::apiResource('/tutors.adverts', App\Http\Controllers\Api\Tutor\TutorAdvertController::class)->only(['index', 'show']);
Route::apiResource('/lessonRequests', App\Http\Controllers\Api\LessonRequest\LessonRequestController::class);
Route::apiResource('/lessonRequests.lessonSessions', App\Http\Controllers\Api\LessonRequest\LessonRequestSessionController::class);
Route::apiResource('/favoriteTutors', App\Http\Controllers\Api\Student\StudentFavoriteTutorController::class)->only(['index', 'store', 'destroy']);
Route::post('/uploadAvatar', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'uploadAvatar']);
Route::get('/removeAvatar', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'removeAvatar']);
Route::post('/uploadIdentification', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'uploadIDDocument']);
Route::patch('/setUserPreference', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'setUserPreference']);
Route::get('/getUserPreference', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'getUserPreference']);
Route::post('/rateTutor', [App\Http\Controllers\Api\Tutor\TutorExtraActionController::class, 'rateTutor']);
Route::apiResource('/tutors.reviews', App\Http\Controllers\Api\Tutor\TutorReviewController::class);
Route::post('/updateProfile', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'profileUpdate']);
Route::get('/userDetail', [App\Http\Controllers\Api\User\UserExtraActionController::class, 'getUserDetail']);
Route::apiResource('/paymentOptions', App\Http\Controllers\Api\PaymentOption\PaymentOptionController::class)->only(['index', 'store', 'show', 'destroy']);
Route::apiResource('/tutors.students', App\Http\Controllers\Api\Tutor\TutorStudentController::class)->only(['index', 'show']);
Route::apiResource('/adverts.lessonRequests', App\Http\Controllers\Api\Advert\AdvertLessonRequest::class)->only(['index']);
Route::get('/lessonRequestStudent/{lessonRequest}', [App\Http\Controllers\Api\LessonRequest\LessonRequestStudent::class, 'index']);

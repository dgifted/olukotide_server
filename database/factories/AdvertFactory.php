<?php

namespace Database\Factories;

use App\Models\Advert;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdvertFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Advert::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $userIds = User::all()->pluck('id')->toArray();
        $subjectIds = Subject::all()->pluck('id')->toArray();

        return [
            'user_id' => random_int(1, floor((count($userIds) / 2))),
            'subject_id' => $this->faker->randomElement($subjectIds),
            'class_type' => $this->faker->randomElement([
                Advert::CLASS_TYPE_INDIVIDUAL,
                Advert::CLASS_TYPE_GROUP
            ]),
            'title' => $this->faker->sentence(random_int(10, 20)),
            'tutor_background_info' => $this->faker->paragraph(random_int(2, 5)),
            'tutor_teaching_method' => $this->faker->paragraph(random_int(2, 5)),
            'platform' => $platform = $this->faker->randomElement([
                Advert::CLASS_PLATFORM_STUDENT,
                Advert::CLASS_PLATFORM_TUTOR,
                Advert::CLASS_PLATFORM_ONLINE
            ]),
            'online_lesson_language' => 'English',
            'transport_fare' => $platform === Advert::CLASS_PLATFORM_STUDENT
                ? $this->faker->randomElement(['1500', '2000', '2500'])
                : null,
            'rate' => $this->faker->randomElement(['2000', '3000', '4000']),
            'tutor_contact_info' => $this->faker->address,
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Advert;
use App\Models\LessonRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class LessonRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LessonRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $userIds = User::all()->pluck('id')->toArray();
        $advertIds = Advert::all()->pluck('id')->toArray();

        return [
            'user_id' => random_int(floor(count($userIds) / 2)  + 1, count($userIds)),
            'advert_id' => $this->faker->randomElement($advertIds)
        ];
    }
}

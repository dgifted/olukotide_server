<?php

namespace Database\Seeders;

use App\Models\LessonRequest;
use App\Models\SubjectCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(50)->create();
         \App\Models\Advert::factory(15)->create();
         LessonRequest::factory(20)->create();
    }
}

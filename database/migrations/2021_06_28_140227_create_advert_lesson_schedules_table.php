<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertLessonSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_lesson_schedules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('advert_id')->constrained();
            $table->enum('week_day', \App\Models\AdvertLessonSchedule::DAYS);
            $table->enum('shift', \App\Models\AdvertLessonSchedule::SHIFTS);
            $table->dateTime('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advert_lesson_schedules');
    }
}

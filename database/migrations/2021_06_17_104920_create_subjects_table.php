<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subject_category_id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->text('description')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        $categories = \App\Models\SubjectCategory::all('id');
        foreach ($categories as $cat) {
            if ($cat->id === 1) {
                $cat->subjects()->create([
                    'name' => 'French'
                ]);
                $cat->subjects()->create([
                    'name' => 'Yoruba'
                ]);
                $cat->subjects()->create([
                    'name' => 'Igbo'
                ]);
                $cat->subjects()->create([
                    'name' => 'Spanish'
                ]);
            }

            if ($cat->id === 2) {
                $cat->subjects()->create([
                    'name' => 'Pre - Kindergarten Maths'
                ]);
                $cat->subjects()->create([
                    'name' => 'Kindergarten Language Arts'
                ]);
                $cat->subjects()->create([
                    'name' => 'Fourth Grade Socials Studies'
                ]);
                $cat->subjects()->create([
                    'name' => 'Third Grade Science'
                ]);
            }

            if ($cat->id === 3) {
                $cat->subjects()->create([
                    'name' => 'Ninth Grade Algebra'
                ]);
                $cat->subjects()->create([
                    'name' => 'Fifth Grade Mathematics'
                ]);
                $cat->subjects()->create([
                    'name' => 'Twelfth Grade Calculus'
                ]);
                $cat->subjects()->create([
                    'name' => 'Sixth Grade Socials Studies'
                ]);
            }

            if ($cat->id === 4) {
                $cat->subjects()->create([
                    'name' => 'IELTS'
                ]);
                $cat->subjects()->create([
                    'name' => 'GRE Analytical writing'
                ]);
                $cat->subjects()->create([
                    'name' => 'GMAT Quantitative Tutor'
                ]);
                $cat->subjects()->create([
                    'name' => 'ICSAN'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}

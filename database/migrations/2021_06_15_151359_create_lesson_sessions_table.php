<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_sessions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lesson_request_id');
            $table->tinyInteger('status')->default(\App\Models\LessonSession::SESSION_PENDING);
            $table->dateTime('scheduled_to_start')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->dateTime('user_joined_at')->nullable();
//            $table->text('feedback_comment')->nullable();
//            $table->string('rating')->nullable(); //TODO Take this out to a separate model
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_sessions');
    }
}

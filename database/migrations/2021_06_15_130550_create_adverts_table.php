<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('subject_id');
            $table->tinyInteger('class_type')->default(\App\Models\Advert::CLASS_TYPE_INDIVIDUAL);
            $table->string('title');
            $table->text('tutor_background_info')->nullable();
            $table->text('tutor_teaching_method')->nullable();
            $table->string('youtube_link')->nullable();
            $table->tinyInteger('platform')->default(\App\Models\Advert::CLASS_PLATFORM_ONLINE);
            $table->string('online_lesson_language')->nullable();
            $table->string('transport_fare')->nullable();
            $table->string('rate')->nullable();
            $table->string('tutor_contact_info')->nullable();
            $table->binary('profile_photo')->nullable();
            $table->tinyInteger('approved')->default(\App\Models\Advert::APPROVAL_PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}

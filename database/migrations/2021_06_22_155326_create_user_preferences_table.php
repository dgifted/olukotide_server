<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_preferences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->tinyInteger('email_notify_activity')->default(\App\Models\UserPreference::SET_OFF);
            $table->tinyInteger('email_notify_lesson_request')->default(\App\Models\UserPreference::SET_OFF);
            $table->tinyInteger('email_notify_offer_ad')->default(\App\Models\UserPreference::SET_OFF);
            $table->tinyInteger('sms_notify_lesson_request')->default(\App\Models\UserPreference::SET_OFF);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_preferences');
    }
}

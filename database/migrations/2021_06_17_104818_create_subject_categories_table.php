<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        \App\Models\SubjectCategory::create([
            'name' => 'Languages',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'EYFS & Junior School',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Middle age & High school',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Professional Exam',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Adult Learning',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Undergraduates',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'WAEC & JAMB',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Programming',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Design',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Extracurricular Activities',
        ]);
        \App\Models\SubjectCategory::create([
            'name' => 'Robotics',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_categories');
    }
}
